import Vue from 'vue'
import Vuex from 'vuex'
import Router from 'vue-router'
import App from './App.vue'
import routes from './router'
import store from './store'
Vue.config.productionTip = false;
let newRouter = new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes
});
let newStore = new Vuex.Store(store);
new Vue({
  router: newRouter,
  store: newStore,
  render: h => h(App)
}).$mount('#app')
