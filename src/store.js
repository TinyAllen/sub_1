import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default {
  state: {
    menu: [{
      title: 'DMP',
      to: '/'
    }]
  },
  mutations: {

  },
  actions: {

  }
}
